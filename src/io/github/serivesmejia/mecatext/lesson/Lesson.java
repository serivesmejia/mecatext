package io.github.serivesmejia.mecatext.lesson;


public class Lesson {
    
    public String[] lines;
    
    public String[] preLesson;
    
    public String lessonName = "default";
    
    public String getLine(int line){
        return lines[line];
    }
    
    public String[] getLines(){
        return lines;
    }
    
    public String[] getPreLesson(){
        return preLesson;
    }
    
    public String getLessonName(){
        return lessonName;
    }
    
    public Lesson(String[] lines, String[] preLesson, String lessonName){
        this.lines = lines;
        this.preLesson = preLesson;
        this.lessonName = lessonName;
    }
    
}
