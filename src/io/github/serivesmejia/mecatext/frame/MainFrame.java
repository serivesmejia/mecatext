package io.github.serivesmejia.mecatext.frame;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;
import static io.github.serivesmejia.mecatext.Main.frame;
import io.github.serivesmejia.mecatext.lesson.Lesson;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author serivesmejia
 */
public class MainFrame extends javax.swing.JFrame{

    String voiceName = "kevin7";
    private Voice voice = VoiceManager.getInstance().getVoice("kevin16");
    
    /**
     * Creates new form MainFrame
     */
    int w;
    List<Integer> wpm = new ArrayList();
    public MainFrame() {
        addKeyListener(getShortcutKeyListener());
        initComponents();
        voice.allocate();
        jPanel1.setVisible(false);
        jMenuItem5.setEnabled(inLesson);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/machine.png")));
       t.schedule(new TimerTask(){
        @Override
        public void run() {
          if(!paused && inLesson){
            time += 1;
            //stringTime = String.valueOf(time)+" s";
            stringTime = LocalTime.MIN.plusSeconds(time).toString();
            jLabel3.setText(stringTime);
            requestFocus();
          
         }
        }
       }, 0, 1000);
        t2.schedule(new TimerTask(){
        @Override
        public void run() {
          if(!paused && inLesson){
            float result = 0;
            wpm.add(w);
            w = 0;
            if(wpm.toArray().length == 0){
                result = wpm.get(0);
            }else{
                int total = 0;
                int c = wpm.toArray().length + 1;
                for(Integer i : wpm){
                    total += i;
                }
                
                result = (float) total / c;
                
            }
            stringPpm = new DecimalFormat("##.#").format(result)+ " PPM";
            jLabel4.setText(stringPpm);
            ppm = result;
          
         }
        }
       }, 0, 60 * 1000);
   }
    
    float ppm = 0f;
    
    String stringPpm = "N/A";
    
    int mistakes = 0;
    int totalTime = 0;
    int totalTypes = 0;
    int time = -1;
    int nolines = 0;
    int totalLines = 0;
    String stringTime;
    
    Timer t = new Timer();
    Timer t2 = new Timer();

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("MecaText - Version 1.0.0");
        setBackground(new java.awt.Color(255, 255, 255));
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                formKeyTyped(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setToolTipText("");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel2.setText("Tiempo total:");

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel3.setText("00:00");

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel6.setText("Teclas presionadas:");

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel7.setText("0");

        jLabel8.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel8.setText("Errores totales:");

        jLabel9.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel9.setText("0");

        jLabel10.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel10.setText("Lineas:");

        jLabel11.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel11.setText("0/0");

        jLabel12.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel12.setText("Calificacion:");

        jLabel13.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel13.setText("0%");

        jLabel14.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel14.setText("Palabras:");

        jLabel15.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel15.setText("0");

        jLabel16.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel16.setText("Pulsaciones/min:");

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel4.setText("N/A");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel11))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel13))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel15))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jLabel4))
                .addGap(43, 43, 43))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(1);
        jTextArea1.setFont(new java.awt.Font("DejaVu Math TeX Gyre", 0, 48)); // NOI18N
        jTextArea1.setForeground(new java.awt.Color(0, 0, 0));
        jTextArea1.setRows(1);
        jScrollPane1.setViewportView(jTextArea1);

        jTextArea2.setEditable(false);
        jTextArea2.setColumns(1);
        jTextArea2.setFont(new java.awt.Font("DejaVu Math TeX Gyre", 0, 48)); // NOI18N
        jTextArea2.setForeground(new java.awt.Color(0, 102, 204));
        jTextArea2.setRows(1);
        jScrollPane2.setViewportView(jTextArea2);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 693, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jMenu2.setText("Usuario");

        jMenuItem1.setText("Iniciar Sesion");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuItem2.setText("Registrar Usuario");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuItem4.setText("Guardar y Salir");
        jMenuItem4.setToolTipText("");
        jMenuItem4.setEnabled(false);
        jMenu2.add(jMenuItem4);

        jMenuBar1.add(jMenu2);

        jMenu1.setText("Leccion");

        jMenuItem3.setText("Seleccionar Leccion");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem5.setText("Pausa");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuItem6.setText("Terminar");
        jMenuItem6.setEnabled(false);
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem6);

        jMenuBar1.add(jMenu1);

        jMenu4.setText("Preferencias");

        jCheckBoxMenuItem1.setText("Dictado (no recomendado)");
        jCheckBoxMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem1ActionPerformed(evt);
            }
        });
        jMenu4.add(jCheckBoxMenuItem1);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(51, 51, 51))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyTyped

    }//GEN-LAST:event_formKeyTyped

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                SelectLessonDialog dialog = new SelectLessonDialog(frame, true);
                dialog.setVisible(true);
            }
        });
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jCheckBoxMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxMenuItem1ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        pause();
    }//GEN-LAST:event_jMenuItem5ActionPerformed
public boolean nextLessonBTT = false;
    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        nextLessonBTT = false;
        endLesson();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                CreateAccountDialog dialog = new CreateAccountDialog(frame, true);
                dialog.setVisible(true);
            }
        });
    }//GEN-LAST:event_jMenuItem2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    // End of variables declaration//GEN-END:variables

    
boolean close = false;
    
String lessonName = "<n/a>";

boolean paused = false;
public void pause(){
    if(!paused){
       if(inLesson){
        running = false;
        paused = true;
        java.awt.EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
            PausedDialog dialog = new PausedDialog(MainFrame.this, true);
            dialog.setVisible(true);
        }
        });
      }
    }else if(inLesson){
        running = true;
        paused = false;
    }
}

public void update(){
    jLabel7.setText(String.valueOf(totalTypes));
    jLabel9.setText(String.valueOf(mistakes));
    jLabel11.setText(String.valueOf(totalLines + 1)+"/"+String.valueOf(nolines - 1));
    jLabel15.setText(String.valueOf(totalWords));
    grade = calcGrade(totalTypes - mistakes, totalTypes);
    if(Float.isNaN(grade)){
        grade = 0;
    }
    this.jLabel13.setText(new DecimalFormat("##.#").format(grade) + "%");
}  

public float calcGrade(int goodTypes, int totalTypes){
    try{
    float a = (float)goodTypes / totalTypes;
    float b = a*100;
    return b;
    }catch (Exception ex){
        
    }
    System.out.println("a");
   return 0f;
}

float grade = 0f;

public boolean inLesson = false;
    
public void beginLesson(Lesson l){
    requestFocus();
    lessonName = l.getLessonName();
    this.setTitle("MecaText - " + lessonName);
    jLabel3.setText("00:00");
    lines = l.lines;
    lineNo = -1;
    charNo = 0;
    mistakes = 0;
    time = -1;
    stringPpm = "N/A";
    ppm = 0f;
    grade = 0f;
    stringTime = "00:00";
    totalTypes = 0;
    totalChar = 0;
    w = 0;
    wpm = new ArrayList();
    nolines =  l.lines.length + 1;
    totalLines = 0;
    text = "";
    jLabel4.setText("N/A");
    totalWords = 0;
    text1 = "";
    running = true;
    jPanel1.setVisible(true);
    
    jMenuItem1.setEnabled(false);
    jMenuItem2.setEnabled(false);
    jMenuItem6.setEnabled(true);
    
    update();
    nextLine();
}
    
char currentChar;
int charNo = 0;
int totalChar;
int totalWords = 0;
String text1;
String currentWord;
int currentWordIndex = 0;

public KeyListener getShortcutKeyListener() {
    KeyListener listener = new KeyListener() {

        @Override
        public void keyPressed(KeyEvent evt) {
            
        }

        @Override
        public void keyTyped(KeyEvent e) {
            
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
                pause();
                System.out.println("pause");
            }else{
                totalTypes += 1; 
                w += 1;
            }
        if(running){
            jLabel1.setText("");
            System.out.println(text.length());
          if(text.length() != totalChar){
           if(currentChar == e.getKeyChar()){
            if(Character.isLetter(e.getKeyChar())){
            text = text + e.getKeyChar();
            jTextArea2.setText(text + "_");
            charNo += 1;
            try{
            currentChar = text1.charAt(charNo);
            }catch(Exception ex){
                
            }
            }else if(e.getKeyCode() == 32){
                text = text + " ";
                
                try{
                currentWordIndex += 1;
                currentWord = text1.split(" ")[currentWordIndex];
                if(jCheckBoxMenuItem1.isSelected()){
                java.awt.EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        voice.speak(currentWord);
                    }
                });
                }
                }catch(Exception ex){
                    
                }
                
                totalWords += 1;
                jTextArea2.setText(text + "_");
                charNo += 1;
                currentChar = text1.charAt(charNo);
            }
          }else if(e.getKeyCode() != KeyEvent.VK_ESCAPE){
               String c;
               if(currentChar == " ".charAt(0)){
                   c = "Space";
               }else{
                   char[] c1 = {currentChar};
                   c = new String(c1);
               }
               
               if(Character.isLetter(e.getKeyChar())){
                  jLabel1.setText("Presiono " + e.getKeyChar() + ", porfavor presione " + c);
               }else{
                  jLabel1.setText("Presiono " + KeyEvent.getKeyText(e.getKeyCode()) + ", porfavor presione " + c); 
               }
               mistakes += 1;
           }
         }else{
              if(e.getKeyCode() == KeyEvent.VK_ENTER){
              nextLine();
              totalLines += 1;
              totalWords += 1;
              }else{
                 jLabel1.setText("Presiono " + KeyEvent.getKeyText(e.getKeyCode()) + ", porfavor presione Enter"); 
                 mistakes += 1;
              }
          }
          update();
        }
        }
    };
    return listener;
}

public String[] lines = {"default"};

int lineNo = -1;

public void nextLine(){
    if(running){
    lineNo += 1;
    try{
    text1 = lines[lineNo];
    }catch(Exception ex){
        endLesson();
        this.nextLessonBTT = true;
        return;
    }
    
    try{
    currentWordIndex = 0;
    currentWord = text1.split(" ")[currentWordIndex];
    if(jCheckBoxMenuItem1.isSelected()){
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                voice.speak(currentWord);
            }
        });
    }
    }catch (Exception ex){
        
    }
    
    text = "";
    jTextArea2.setText(text);
    jTextArea1.setText(text1);
    totalChar = text1.length();
    charNo = 0;
    inLesson = true;
    paused = false;
    jMenuItem5.setEnabled(inLesson);
    currentChar = text1.charAt(0);
    }
    
}

public void endLesson(){
    jTextArea2.setText("");
    jTextArea1.setText("");
    running = false;
    this.setTitle("MecaText - 1.0.0");
    inLesson = false;
    jMenuItem5.setEnabled(inLesson);
    jPanel1.setVisible(false);
    jLabel1.setText("");
    jMenuItem1.setEnabled(true);
    jMenuItem2.setEnabled(true);
    jMenuItem6.setEnabled(false);
   java.awt.EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
            EndLessonDialog dialog = new EndLessonDialog(MainFrame.this, true);
            dialog.setVisible(true);
        }
    });
}

public boolean running = false;

public String text = "";

}
