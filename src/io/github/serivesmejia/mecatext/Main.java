package io.github.serivesmejia.mecatext;

import io.github.serivesmejia.mecatext.frame.MainFrame;
import io.github.serivesmejia.mecatext.frame.SelectLessonDialog;
import io.github.serivesmejia.mecatext.lesson.Lesson;
import io.github.serivesmejia.mecatext.user.User;
import io.github.serivesmejia.mecatext.util.MinLog;
import io.github.serivesmejia.mecatext.util.TrippleDes;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author serivesmejia
 */
public class Main {

    public static MainFrame frame;
    public static List<Lesson> lessons = new ArrayList();
    
    public static boolean isLoggedIn = false;
    public static User loggedUser;
    
    public static File gameSaveFolder = new File(defaultDirectory() + "/.MecaText");
    public static File dataFile = new File(gameSaveFolder + "/data.json");
    public static File usersFolder = new File(gameSaveFolder + "/users");
    
    public static void main(String[] args) {
        String encrypted = new TrippleDes(TrippleDes.toKey("encryptTest01")).encrypt("encryptTest");
        String decrypted = new TrippleDes(TrippleDes.toKey("encryptTest01")).decrypt(encrypted);
        System.out.println(encrypted);
        System.out.println(decrypted);
        
        gameSaveFolder.mkdirs();
        usersFolder.mkdirs();
        
        MinLog.info("Main", "Initalizing MecaText v1.0.0");
        MinLog.info("");
        MinLog.info("Main", "Loading lessons...");
        String[] lines = {"asdf asdf asdf asdf asdf", "fdsa fdsa fdsa daniela","asdf asdf asdf asdf asdf", "fdsa fdsa fdsa daniela","asdf asdf asdf asdf asdf", "fdsa fdsa fdsa daniela","asdf asdf asdf asdf asdf", "fdsa fdsa fdsa daniela","asdf asdf asdf asdf asdf", "fdsa fdsa fdsa daniela","asdf asdf asdf asdf asdf", "fdsa fdsa fdsa daniela","asdf asdf asdf asdf asdf", "fdsa fdsa fdsa daniela","asdf asdf asdf asdf asdf", "fdsa fdsa fdsa daniela","asdf asdf asdf asdf asdf", "fdsa fdsa fdsa daniela",};
        String[] pre = {"preTest01","preTest02","preTest03"};
        Lesson l = new Lesson(lines, pre, "Test Lesson");
        lessons.add(l);
        MinLog.info("");
        MinLog.info("Main", "Creating JFrame MainFrame");
        frame = new MainFrame();
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.requestFocus();
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                SelectLessonDialog dialog = new SelectLessonDialog(frame, true);
                dialog.setVisible(true);
            }
        });
    }
    
public static String defaultDirectory()
{
    String OS = System.getProperty("os.name").toUpperCase();
    if (OS.contains("WIN")){
        System.out.println("home: " + System.getenv("APPDATA"));
        return System.getenv("APPDATA");
    }else if (OS.contains("MAC")){
        System.out.println("home: "+ System.getProperty("user.home") + "/Library/Application "
                + "Support");
        return System.getProperty("user.home") + "/Library/Application "
                + "Support";
    }else if (OS.contains("NUX")){
        System.out.println("home: " + System.getProperty("user.home"));
        return System.getProperty("user.home");
    }
    return System.getProperty("user.dir");
}
    
    public static Lesson getByName(String lessonName){
        for(Lesson l : lessons){
            if(l.lessonName == lessonName){
                return l;
            }
        }
        return null;
    }
    
}
