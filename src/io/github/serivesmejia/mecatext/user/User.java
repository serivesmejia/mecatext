package io.github.serivesmejia.mecatext.user;

import com.google.gson.Gson;
import io.github.serivesmejia.mecatext.util.TrippleDes;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class User {
    
    public int permLevel;
    
    public UUID usrKey;
    public String usrName;
    
    public Session session;
    
    public List<LessonData> lessonData = new ArrayList();
    
    public User(int permLevel, UUID usrKey){
        this.permLevel = permLevel;
        this.usrKey = usrKey;
        usrKey = UUID.randomUUID();
    }
    
    public String toEncryptedJson(String passphrase){
        Gson gson = new Gson();
        return new TrippleDes(TrippleDes.toKey(passphrase))
                              .encrypt(gson.toJson(this));
    }
    
    public static User fromEncryptedJson(String passphrase, String encryptedJson){
        Gson gson = new Gson();
        try{
        User usr = gson.fromJson(new TrippleDes(TrippleDes.toKey(passphrase))
                              .decrypt(encryptedJson), 
                              User.class);
        return usr;
        }catch (Exception ex){
            return null; //If passphrase is incorrect (generally), we'll reach here.
        }
    }
    
}
