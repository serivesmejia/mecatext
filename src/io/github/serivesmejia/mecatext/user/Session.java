package io.github.serivesmejia.mecatext.user;

/**
 *
 * @author serivesmejia
 */
public class Session {
    
    public int lastLine;
    public int lastCharIndex;
    public String lastLesson;
    
    float ppm = 0f;
    
    String stringPpm = "N/A";
    
    int mistakes = 0;
    int totalTime = 0;
    int totalTypes = 0;
    int time = -1;
    int nolines = 0;
    int totalLines = 0;
    String stringTime;
    
}
