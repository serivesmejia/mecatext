package io.github.serivesmejia.mecatext.user;


public class LessonData {
    
    public String totalTime;
    public String pressedKeys;
    public String totalMistakes;
    public String totalWords;
    public String avgPpm;
    public String finalGrade;
    public String lessonName;
    
    public LessonData(String totalTime, String pressedKeys, String totalMistakes, String totalWords, String avgPpm, String finalGrade, String lessonName){
        this.totalTime = totalTime;
        this.pressedKeys = pressedKeys;
        this.totalMistakes = totalMistakes;
        this.totalWords = totalWords;
        this.avgPpm = avgPpm;
        this.finalGrade = finalGrade;
        this.lessonName = lessonName;
    }
    
    
}
