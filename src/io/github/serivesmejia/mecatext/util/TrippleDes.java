package io.github.serivesmejia.mecatext.util;

import java.nio.charset.Charset;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

/**
 *
 * @author serivesmejia
 */
public class TrippleDes {
    
    private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private KeySpec ks;
    private SecretKeyFactory skf;
    private Cipher cipher;
    byte[] arrayBytes;
    private byte[] myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;
    
    public TrippleDes(byte[] encryptionKey){
        myEncryptionKey = encryptionKey;
        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
        
        try{
        arrayBytes = myEncryptionKey;
        ks = new DESedeKeySpec(arrayBytes);
        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
        cipher = Cipher.getInstance(myEncryptionScheme);
        key = skf.generateSecret(ks);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }


    public String encrypt(String unencryptedString) {
        String encryptedString = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString = new String(Base64.encodeBase64(encryptedText));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedString;
    }


    public String decrypt(String encryptedString) {
        String decryptedText=null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText= new String(plainText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedText;
    }
    
    public static byte[] toKey(String s){
        if(s.length() == 24){
            return s.getBytes(Charset.defaultCharset());
        }else if(s.length() > 24 ){
           char[] c = s.toCharArray();
           List<Character> l = new ArrayList();
           int count = 0;
           for(char c1 : c){
               count += 1;
               if(count != 25){
                   l.add(c1);
               }else{
                   break;
               }
           }
           
            StringBuilder sb = new StringBuilder();
            for(Character ch : l){
                sb.append(ch.charValue());
            }
            return sb.toString().getBytes(Charset.defaultCharset());
        }else if(s.length() < 24 ){
            int missingChars = 24 - (s.length());
            String new1 = s;
            while(missingChars != 0){
                missingChars -= 1;
                new1 += "4";
            }
            return new1.getBytes(Charset.defaultCharset());
        }
        return null;
    }
}
